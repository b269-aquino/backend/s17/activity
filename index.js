/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:

function printWelcomeMessage(){
	let userName = prompt("What is your name?");
	let userAge = prompt("What is your age?");
	let userAddress = prompt("Where do you live?");
	console.log("Hello, " + userName);
	console.log("You are " + userAge + "\ years old.");
	console.log("You live in " + userAddress);
}
printWelcomeMessage();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

function getBandName(){
	let bandName1 = "1. Eraserheads";
	let bandName2 = "2. Ben&Ben";
	let bandName3 = "3. Parokya ni Edgar";
	let bandName4 = "4. Kamikazee";
	let bandName5 = "5. Hale";
	console.log(bandName1);
	console.log(bandName2);
	console.log(bandName3);
	console.log(bandName4);
	console.log(bandName5);
}
getBandName();


/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

function getMovieTitles(){
	let movieTitle1 = "1. Harry Potter and The Sorcerer's Stone";
	let movieRating1 = "Rotten Tomatoes Rating: 81%";

	let movieTitle2 = "2. John Wick";
	let movieRating2 = "Rotten Tomatoes Rating: 92%";

	let movieTitle3 = "3. Spider Man No-way-Home";
	let movieRating3 = "Rotten Tomatoes Rating: 93%";

	let movieTitle4 = "4. The Greatest Showman";
	let movieRating4 = "Rotten Tomatoes Rating: 56%";

	let movieTitle5 = "5. Conjuring";
	let movieRating5 = "Rotten Tomatoes Rating: 80%";
	console.log(movieTitle1 + "\n" + movieRating1);
	console.log(movieTitle2 + "\n" + movieRating2);
	console.log(movieTitle3 + "\n" + movieRating3);
	console.log(movieTitle4 + "\n" + movieRating4);
	console.log(movieTitle5 + "\n" + movieRating5);

}
getMovieTitles();
/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/


// ACTIVITY 2

function printFriends(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
